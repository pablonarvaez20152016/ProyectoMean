const express = require('express');
const router = express.Router();
const employeeCtrl = require('../controllers/employees.controller');


router.get('/',employeeCtrl.getEmployees);// cuando el navegador trate de dar una
// peticion get a la ruta '/' se responde con la funcion getemployees
router.post('/',employeeCtrl.createemployee);
router.get('/:id',employeeCtrl.getEmployee);
router.put('/:id',employeeCtrl.editemployee);
router.delete('/:id',employeeCtrl.deletemployee);
module.exports = router;