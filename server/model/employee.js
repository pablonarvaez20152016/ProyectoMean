var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const EmployeeSchema= new Schema({
    name:{type: String, required:true },
    position:{ type: String, required: true },
    office:{ type: String, required: false },
    salary:{ type: Number,required:true}
});

module.exports = mongoose.model('Employee', EmployeeSchema);