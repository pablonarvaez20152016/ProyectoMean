const Employee = require('../model/employee')
const employeeCtrl = {};
const mongoose = require('mongoose');
const { param } = require('../routes/employees.routes');


employeeCtrl.getEmployees = async (req, res) => {
    const employees = await Employee.find();
    res.json(employees);

}

employeeCtrl.createemployee = async (req, res) => {  

    const employee =  new Employee({
        name:req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary
    });
    await employee.save();
    res.json({
        'status':'Employee saved'
    });
    
}

employeeCtrl.getEmployee = async (req,res)=>{
    //console.log(req.params); //mueestra los parametros que tiene la request
    const employee= await Employee.findById(req.params.id);
    res.json(employee);
}

employeeCtrl.editemployee = async(req, res) => {
    const { id } = req.params;
    const employee = {
        name: req.body.name,
        position: req.body.position,
        oficce: req.body.oficce,
        salary: req.body.salary
    };
    await Employee.findByIdAndUpdate(id,{$set:employee},{new:true});
    res.json({
        'status':'Employee updated'
    });
}
employeeCtrl.deletemployee = async(req, res) => {
    await Employee.findByIdAndDelete(req.params.id);
    res.json({
        'status':'Employee delete'
    });
    
}

module.exports = employeeCtrl;