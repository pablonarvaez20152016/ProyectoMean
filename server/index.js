/*  link video : https://www.youtube.com/watch?v=khCIunNAEHI
para comentar varias lineas : shitf + alt + A */

const express =require('express');
const morgan = require('morgan');
const app = express();
const https = require('https');

//es un middleraware que permite reconocer el servidor del frontend
const cors = require('cors');







const { mongoose} = require('./database');

const routes = require('./routes/employees.routes');

// settings
app.set('port', process.env.PORT || 3000); //crea variable port y le asigna el valor 3000

//middlewares

app.use(morgan('dev'));
app.use(express.json());//linea para que el servidor entienda json
app.use(cors({ origin:'http://localhost:4200'}));

//Routes
app.use('/api/employees',routes);  //app.use(routes)


// Starting the server
const server = app.listen(app.get('port'),()=>{
    console.log('server on port',app.get('port'));
}); 


//jsreport
const reportingApp = express();
app.use('/reporting', reportingApp);



const jsreport = require('jsreport')({
  extensions: {
      express: { app: reportingApp, server: server },
  },
  appPath: "/reporting"
});

jsreport.init().then(() => {
  console.log('jsreport server started')
}).catch((e) => {
  console.error(e);
});




//database






/* const mongoUtils = require("./mongoUtils");
const mongo = new mongoUtils();

(async() => {
    let insert = await mongo.insert("Ejecutando mongo desde nodeJS");
    console.log(insert);
})(); */